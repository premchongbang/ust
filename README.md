# README #

This application uses Node.js for executing server-side code and PostGreSql for creating database.

### How to set up and run locally? ###

# Installation Required #
Node.js 8.1
PgAdmin 3

# Dependencies #
Need to install Node.js package manager(npm) for installing these packages.(for instance, npm install pg)

"async": "^2.5.0",
"body-parser": "^1.17.2",
"consolidate": "^0.14.5",
"express": "^4.15.4",
"html": "^1.0.0",
"pg": "^7.2.0",
"socket.io": "^2.0.3",
"swig": "^1.4.2"

# Database configuration #
Install PgAdmin3.
Create a batabase server on PgAdmin3. (Tutorial Link- https://www.youtube.com/watch?v=1wvDVBjNDys&t=374s)
Create a database to store the data.
Run the script (located at file folder in txt format) to create a table using SQL editor.
Import a data using csv file (located at file folder) into the table.
Set the database configuration at app.js file according to Pgadmin DB server.

# Deployment instructions #
Using terminal, type "node app.js" to run the server which runs the application on port 8081. (Link - http://localhost:8081/)